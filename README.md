## Symfony image

Base image for symfony application.

#### Ubuntu 18.04

**Software:**

* git
* curl

#### PHP 7.2

**Database drivers:**
* Mysql
* SQLite
* PostgreSQL
* MongoDB
* Redis

**Modules:**

* amqp
* calendar
* Core
* ctype
* curl
* date
* dba
* dom
* enchant
* exif
* fileinfo
* filter
* ftp
* gd
* geoip
* gettext
* gmp
* gnupg
* hash
* http
* iconv
* igbinary
* imagick
* imap
* interbase
* intl
* json
* ldap
* libsmbclient
* libxml
* mailparse
* mbstring
* memcache
* memcached
* mongodb
* msgpack
* mysqli
* mysqlnd
* OAuth
* odbc
* openssl
* pcntl
* pcre
* PDO
* pdo_dblib
* PDO_Firebird
* pdo_mysql
* PDO_ODBC
* pdo_pgsql
* pdo_sqlite
* pgsql
* Phar
* posix
* propro
* pspell
* radius
* raphf
* readline
* recode
* redis
* Reflection
* rrd
* session
* shmop
* SimpleXML
* smbclient
* snmp
* soap
* sockets
* sodium
* solr
* SPL
* sqlite3
* standard
* Stomp
* sysvmsg
* sysvsem
* sysvshm
* tidy
* tokenizer
* uploadprogress
* uuid
* wddx
* xdebug
* xml
* xmlreader
* xmlwriter
* xsl
* yaml
* Zend OPcache
* zip
* zlib
* zmq

**Check PHP**

```
php -v
php -m
```

#### Composer 1.8.0

```
composer install
```

Enjoy!